import React, { Fragment } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import { ArrowDropDown } from '@material-ui/icons';
import { Button, Menu, MenuItem } from '@material-ui/core';



class RowFiles extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      files: this.props.files,
      name: this.props.name,
    };
  }
  
  componentDidMount() {
    fetch('http://127.0.0.1:2000/', {
      method: 'GET',
      cache: 'no-cache'
    })
    .then(response => {
      return response.json();
      })
    .then(result => {
      this.setState({files: JSON.parse(result)});
      })
    .catch(err => alert(err));
    
  }
  
  handleMenuClick(event) {
    this.setState({ anchorEl: event.currentTarget});
    event.stopPropagation();
  };
  
  handleClose(event) {
    this.setState({anchorEl: null });
    event.stopPropagation();
  };
  
  handleDelete(index, event) {
    const { files } = this.state;
    files.splice(index, 1);
    this.setState({ files: files, anchorEl: null });
    event.stopPropagation();
  }
  
  handleOpen(file) {
    if (file.type === 'directory') {
      this.setState({ files: file.meta, name: file.name });
    }
    if (file.type === 'file') {
      this.setState({ files: null, name: file.name, meta: file.meta })
    }
  }
  
  onClickDontWork(e) {
    alert('не реализовано');
    e.stopPropagation();
  }
  
  render() {
    const { anchorEl, files, name, meta } = this.state;
    return (
      <Fragment>
        <h3>Открыто: {name}</h3>
        {files ?
        <Row className="file-head">
          <Col md={1}></Col>
          <Col md={5}>Имя Файла</Col>
          <Col md={3}>Размер Файла</Col>
          <Col md={3}>Изменено</Col>
        </Row> :
        <div className="file-meta">{meta}</div>}
        {files && files.map((file, id) => {
          return (
            <Row key={id} className="file-item" onClick={() => this.handleOpen(file)}>
              <Col md={1} className="file">
                <Form.Check type="checkbox" />
                <Button className="menu-hover" aria-controls="simple-menu" aria-haspopup="true" onClick={(e) => this.handleMenuClick(e)}>
                  <ArrowDropDown />
                </Button>
                <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={!!anchorEl}
                  onClose={(e) => this.handleClose(e)}
                >
                  <MenuItem onClick={this.handleDelete.bind(this, id)}>Удалить</MenuItem>
                  <MenuItem onClick={(e) => this.onClickDontWork(e)}>Кнопка 1</MenuItem>
                  <MenuItem onClick={(e) => this.onClickDontWork(e)}>Кнопка 2</MenuItem>
                </Menu>
              </Col>
              <Col md={5} className="file">{file.name}</Col>
              <Col md={3}className="file">{file.size}</Col>
              <Col md={3} className="file">{file.updateAt}</Col>
            </Row>
          );
        })}
        <a className="back-button" href="/">Корневой каталог</a>
      </Fragment>
    )
  }
}

export default RowFiles;
