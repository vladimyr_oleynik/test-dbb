import React from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Row, Col, Image } from 'react-bootstrap';
import { Route, Switch, BrowserRouter } from 'react-router-dom'

import './App.css';
import RowFiles from './rowFiles'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      name: 'PublicFolder',
      files: [
        {
          name: 'first.txt',
          size: 134,
          updateAt: '29/01/2020',
          type: 'file',
          meta: {},
        },
        {
          name: 'second.txt',
          size: 264,
          updateAt: '28/01/2020',
          type: 'file',
          meta: {},
        },
      ],
      };
  }
  
  onClickDontWork() {
    alert('не реализовано');
  }
  
  render() {
    return (
      <React.Fragment>
        <Row>
          <Col md={3}>
            <Image src={logo} roundedCircle />
          </Col>
          <Col md={9}>
            <h1>Тестовое задание аналог фаловой системы</h1>
            <Row className="button-group">
              <Button onClick={this.onClickDontWork} variant="success">Добавить</Button>
              <Button onClick={this.onClickDontWork} variant="warning">Информация</Button>
              <Button onClick={this.onClickDontWork} variant="danger">Кнопка</Button>
            </Row>
          </Col>
        </Row>
        <BrowserRouter>
          <Switch>
            <Route path="/" render={() => ( <RowFiles {...this.state}/>)} />
          </Switch>
        </BrowserRouter>
      </React.Fragment>
    );
  }
}

export default App;
