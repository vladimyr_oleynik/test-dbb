const express = require("express");
const app = express();
const fs = require('fs');

const getInfoDir = (dir, path) => {
  return dir.map(fileName => {
    try {
      const file = fs.statSync(`${path}/${fileName}`);
      if (file.isFile()) {
        const fileMeta = fs.readFileSync(`${path}/${fileName}`);
        return {
          name: fileName,
          size: file.size,
          updateAt: file.ctime,
          type: 'file',
          meta: fileMeta.toString(),
        };
      }
      const intoDir = fs.readdirSync(`${path}/${fileName}`);
      return {
        name: fileName,
        size: file.size,
        updateAt: file.ctime,
        type: 'directory',
        meta: getInfoDir(intoDir, `${path}/${fileName}`),
      };
    } catch (err) {
      console.log(err);
    }
    
  });
}




app.use("/", (request, response) => {
  const nameFolder = request.params.path;
  try {
    const dir = fs.readdirSync(`PublicFolder`);
    const res = JSON.stringify(getInfoDir(dir, 'PublicFolder'));
    console.log('был запрос');
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.status('200').json(res);
  } catch (err) {
    console.log(err);
  }
  
  
});
 
app.listen(2000, () => {
    console.log("Сервер начал прослушивание запросов на порту 2000");
});
