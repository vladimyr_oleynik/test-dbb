This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Загрузка

Склонируйте репозиторий 
### `git clone  https://vladimyr_oleynik@bitbucket.org/vladimyr_oleynik/test-dbb.git`

или скачайте архив

## Запуск

Перейдите в каталог "test-dbb"

### `npm start`

перейдите в каталог "server"

### `npm start`

Приложение будет по адрессу http://localhost:3000/
